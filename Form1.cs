﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tomypoli_20
{
   
    public partial class Form1 : Form
    {
        bool cardc = false;
        private string[] picarr = {"10_of_clubs.png", 
       "10_of_diamonds.png",
"10_of_hearts.png",
"10_of_spades.png",
"2_of_clubs.png",
"2_of_diamonds.png",
"2_of_hearts.png",
"2_of_spades.png",
"3_of_clubs.png",
"3_of_diamonds.png",
"3_of_hearts.png",
"3_of_spades.png",
"4_of_clubs.png",
"4_of_diamonds.png",
"4_of_hearts.png",
"4_of_spades.png",
"5_of_clubs.png",
"5_of_diamonds.png",
"5_of_hearts.png",
"5_of_spades.png",
"6_of_clubs.png",
"6_of_diamonds.png",
"6_of_hearts.png",
"6_of_spades.png",
"7_of_clubs.png",
"7_of_diamonds.png",
"7_of_hearts.png",
"7_of_spades.png",
"8_of_clubs.png",
"8_of_diamonds.png",
"8_of_hearts.png",
"8_of_spades.png",
"9_of_clubs.png",
"9_of_diamonds.png",
"9_of_hearts.png",
"9_of_spades.png",
"ace_of_clubs.png",
"ace_of_diamonds.png",
"ace_of_hearts.png",
"ace_of_spades2.png",
"card back blue.png",
"card back red.png",
"jack_of_clubs2.png",
"jack_of_diamonds2.png",
"jack_of_hearts2.png",
"jack_of_spades2.png",
"king_of_clubs2.png",
"king_of_diamonds2.png",
"king_of_hearts2.png",
"king_of_spades2.png",
"queen_of_clubs2.png",
"queen_of_diamonds2.png",
"queen_of_hearts2.png",
"queen_of_spades2.png" };
        NetworkStream clientStream;
        TcpClient client;
        bool DoWork = true;
        string enemypointscard;
        int enemypointscardINT;
        string cardnum = "";
        System.Windows.Forms.PictureBox enemyic = new PictureBox();
        private PictureBox[] pictures;
        public Form1()
        {

            InitializeComponent();
            pictures = new PictureBox[52];
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread myThread = new Thread(connect);
          //  button1.Enabled = false;
            myThread.Start();
            yourscore.Text = "0";
            enemyscore.Text = "0";



        }
        public void OnProcessExit(object sender, EventArgs e)
        {
            
            Environment.Exit(Environment.ExitCode);
            write("2000");
            MessageBox.Show("your score: " + yourscore.Text + "Enemy score: " + enemyscore.Text);
        }

        public void connect() 
        {
           
            try //connect part
            {
                client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error ->" + e);
            }
            do //listen Part
            {
                try
                {
                    // listen();
                    Thread.Sleep(2000);
                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);
                    if (input.Equals("0000"))
                    {
                        button1.Enabled = true;
                    }
                    else
                    {
                        SpinWait.SpinUntil(() => cardc);


                        if (input.Equals("102C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[4], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("110C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[0], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("110D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[1], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("110H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[2], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("110S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[3], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("102D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[5], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("102H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[6], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("102S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[7], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("103C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[8], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("103D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[9], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("103H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[10], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("103S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[11], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("104C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[12], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("104D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[13], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("104H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[14], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("104S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[15], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("105C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[16], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("105D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[17], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("105H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[18], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("105S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[19], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("106C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[20], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("106D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[21], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("106H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[22], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("106S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[23], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("107C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[24], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("107D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[25], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("107H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[26], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("107S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[27], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("108C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[28], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("108D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[29], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("108H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[30], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("108S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[31], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("109C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[32], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("109D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[33], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("109H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[34], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("109S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[35], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("101C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[36], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("101D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[37], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("101H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[38], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("101S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[39], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("111C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[42], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("111D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[43], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("111H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[44], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("111S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[45], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("113C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[46], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("113D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[47], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("113H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[48], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("113S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[49], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("112C"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[50], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("112D"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[51], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("112H"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[52], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("112S"))
                        {
                            Bitmap image1 = (Bitmap)Image.FromFile(picarr[53], true);
                            Invoke((MethodInvoker)delegate { enemyic.Image = image1; });
                        }
                        else if (input.Equals("2000"))
                        {
                            Environment.Exit(Environment.ExitCode);
                            Application.Exit();
                        }
                        enemypointscard = input.Substring(1, 2);
                        enemypointscardINT = Int32.Parse(enemypointscard);
                        int x;
                        try
                        {

                            x = Int32.Parse(cardnum);
                        }
                        catch
                        {
                            x = 0;
                        }
                        if (x > enemypointscardINT)
                        {
                            int scoreint = Int32.Parse(yourscore.Text) + 1;
                            yourscore.Text = scoreint.ToString();
                        }
                        else if (x < enemypointscardINT)
                        {
                            int scoreint = Int32.Parse(enemyscore.Text) + 1;
                            enemyscore.Text = scoreint.ToString();
                        }
                        cardc = false;
                    }
                }
                catch (Exception e)
                {
                    // MessageBox.Show("Error ->" + e);

                }
            } while (true && DoWork);
            
        } // function working
        public void GenerateCards()
        {
            for (var i = 0; i <= 52; i++)
            {
                var newPictureBox = new PictureBox();
                newPictureBox.Width = 75;
                newPictureBox.Height = 100;
            }
            Point nexlocation = new Point(label3.Location.X, label3.Location.Y );
            Point enemylocation = new Point(label4.Location.X, label4.Location.Y );
            enemyic.Name = "enemy";
            enemyic.Image = global::Tomypoli_20.Properties.Resources.card_back_blue;
            enemyic.Location = enemylocation;
            enemyic.Size = new System.Drawing.Size(100, 114);
            enemyic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Controls.Add(enemyic);
            for (int i =0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox curric = new PictureBox();
                curric.Name = "picdynamic" + i;
                curric.Image = global::Tomypoli_20.Properties.Resources.cbr;
                curric.Location = nexlocation;
                curric.Size = new System.Drawing.Size(100, 114);
                curric.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
                curric.Click += delegate (object sender1, EventArgs e1)
                {
                    string currindex = curric.Name.Substring(curric.Name.Length - 1);
                    MessageBox.Show("you have clicked card #" + currindex);
                    ((PictureBox)sender1).Hide();
                    //((PictureBox)sender1).Dispose();
                    Random random = new Random();
                    int rndpic = random.Next(0, picarr.Length);
                    string newpic = picarr[rndpic];
                    Bitmap image1 = (Bitmap)Image.FromFile(newpic, true);
                    curric.Image = image1;
                    ((PictureBox)sender1).Show();
                    this.Controls.Add(curric);
                    string Cardtype = "";
                    cardnum = "";
                    if (newpic.Contains("ace") && newpic.Contains("clubs"))
                    {
                        Cardtype = "C";
                        cardnum = "01";
                        
                    }
                    else if (newpic.Contains("ace") && newpic.Contains("diamonds"))
                    {
                        Cardtype = "D";
                        cardnum = "01";
                    }
                    else if (newpic.Contains("ace") && newpic.Contains("sades"))
                    {
                        Cardtype = "S";
                        cardnum = "01";
                    }
                    else if (newpic.Contains("jack") && newpic.Contains("clubs"))
                    {
                        Cardtype = "C";
                        cardnum = "11";
                    }
                    else if (newpic.Contains("jack") && newpic.Contains("diamonds"))
                    {
                        Cardtype = "D";
                        cardnum = "11";
                    }
                    else if (newpic.Contains("jack") && newpic.Contains("hearts"))
                    {
                        Cardtype = "H";
                        cardnum = "11";
                    }
                    else if (newpic.Contains("jack") && newpic.Contains("spades"))
                    {
                        Cardtype = "S";
                        cardnum = "11";
                    }
                    else if (newpic.Contains("king") && newpic.Contains("clubs"))
                    {
                        Cardtype = "C";
                        cardnum = "13";
                    }
                    else if(newpic.Contains("king") && newpic.Contains("diamonds"))
                    {
                        Cardtype = "D";
                        cardnum = "13";
                    }
                    else if (newpic.Contains("king") && newpic.Contains("hearts"))
                    {
                        Cardtype = "H";
                        cardnum = "13";
                    }
                    else if (newpic.Contains("king") && newpic.Contains("spades"))
                    {
                        Cardtype = "S";
                        cardnum = "13";
                    }
                    else if (newpic.Contains("queen") && newpic.Contains("clubs"))
                    {
                        Cardtype = "C";
                        cardnum = "12";
                        
                    }
                    else if (newpic.Contains("queen") && newpic.Contains("diamonds"))
                    {
                        Cardtype = "D";
                        cardnum = "12";
                    }
                    else if (newpic.Contains("queen") && newpic.Contains("hearts"))
                    {
                        Cardtype = "H";
                        cardnum = "12";
                    }
                    else if (newpic.Contains("queen") && newpic.Contains("spades"))
                    {
                        Cardtype = "S";
                        cardnum = "12";

                    }
                    else if (newpic.Contains("clubs"))
                    {
                        Cardtype = "C";
                        cardnum = newpic[0].ToString();
                        if (cardnum.Length < 2)
                        {
                            cardnum = "0" + cardnum;
                        }
                    }
                    else if (newpic.Contains("diamonds"))
                    {
                        Cardtype = "D";
                        cardnum = newpic[0].ToString();
                        if (cardnum.Length < 2)
                        {
                            cardnum = "0" + cardnum;
                        }
                    }
                    else if (newpic.Contains("hearts"))
                    {
                        Cardtype = "H";
                        cardnum = newpic[0].ToString();
                        if (cardnum.Length < 2)
                        {
                            cardnum = "0" + cardnum;
                        }
                    }
                    else if (newpic.Contains("spades"))
                    {
                        Cardtype = "S";
                        cardnum = newpic[0].ToString();
                        if (cardnum.Length < 2)
                        {
                            cardnum = "0" + cardnum;
                        }
                    }
                    //string send = cardnum + Cardtype;
                    write("1" + cardnum.ToString() + Cardtype.ToString()); 
                    
                };
                this.Controls.Add(curric);
                nexlocation.X += curric.Size.Width + 6;
                if(nexlocation.X > this.Size.Width)
                {
                    nexlocation.X = button1.Location.X;
                    nexlocation.Y += curric.Size.Width + 6;
                }
            }
        }
        public void write(string s)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(s);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            cardc = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
          GenerateCards();
          
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
            write("2000");
            MessageBox.Show("your score: " + yourscore.Text + "Enemy score: " + enemyscore.Text);
        }
    }
}
